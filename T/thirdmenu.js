
window.onload = function () {

    /*
    * 每一个菜单都是一个div，
    *   当div具有collapsed这个类时，div就是折叠的状态
    *    当div没有这个类时，div就是展开状态
    * */

    /*
    * 实现功能
    *     点击菜单，切换菜单的显示状态
    * */
    var menuSpan = document.querySelectorAll(".menuSpan");//需要css选择,点是必须的，没有点是元素选择器

    //定义一个变量，保存当前打开的菜单
    var openDiv = menuSpan[0].parentNode;//第一个div，即第一个span的父元素

    //为span绑定单击响应函数
    for (var i = 0 ; i < menuSpan.length ; i++){
        menuSpan[i].onclick = function () {

            //this代表当前点击的span
            //获取当前span的父元素
            var parentDiv = this.parentNode;

            //切换折叠显示状态
            toggleMenu(parentDiv);

            //判断openDiv和 parentDiv是否相同
            //只有一添加功能，即如果他有这个类了【即hasClass(openDiv,"collapsed")】的话就不进入判断，没有【即!hasClass(openDiv,"collapsed")】在进入判断加上属性
            if (openDiv != parentDiv  && !hasClass(openDiv,"collapsed")){
                //打开另一个菜单之后，关闭正打开的菜单
                //为了可以统一处理过渡效果，希望将 addClass 改为 toggleClass
                //addClass(openDiv,"collapsed");
                //此处toggleClass不需要有移除功能
                //toggleClass(openDiv,"collapsed");
                //切换菜单显示状态
                toggleMenu(openDiv);
            }

            //修改openDiv为当前大开的div
            openDiv = parentDiv;
        };
    }

    /*
    * 功能：切换折叠显示状态
    * 参数：
    *    obj 要切换的菜单名
    * */
    function toggleMenu(obj) {
        //在切换之前获取元素的高度
        var begin = obj.offsetHeight;

        //关闭parentdiv
        toggleClass(obj,"collapsed");

        //在切换之后获取一个高度
        var end = obj.offsetHeight;

        //console.log("begin = "+begin  + " end = "+  end);
        //动画效果就是将高度从begin到end过渡
        //将元素的高度重置为begin
        obj.style.height = begin + "px";

        //执行动画，从begin到end过渡
        move(obj,"height",end,10,function(){
            //动画执行完毕，内联样式没有存在意义，直接删除
            obj.style.height = "";
        });
    }
};
