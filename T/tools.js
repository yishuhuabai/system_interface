//尝试创建一个可以执行简单动画的函数
/*
* 参数
*  obj     要执行简单动画的对象
*  attr    要执行动画的样式，比如left top height
*  target  执行动画的目标位置
*  speed   移动的速度（正数向右移动，负数向左移动）
*  callback 回调函数，传一个回调函数，会在动画执行完毕之后执行
* */
function move(obj,attr,target,speed,callback) {

    //关闭上一个开启的定时器，否则总按按钮就会总是多开定时器，速度会改变
    clearInterval(obj.timer);

    //获取元素目前的位置
    var current = parseInt(getStyle(obj,attr));

    //判断速度的正负值
    //0向800移动，speed则为正值
    //800向0移动，speed则为负值
    if(current > target){
        //此时速度应为负值
        speed = - speed;
    }

    //开启定时器执行动画效果
    obj.timer =  setInterval(function () {

        //获取box1原来的值
        var oldValue = parseInt(getStyle(obj,attr));
        //在旧值的基础上增加
        var newValue = oldValue + speed;

        //判断newValue是否大于等于800,或者向左移动时判断是否小于0
        //向左移动时，需要判断 newValue 是否小于target
        //向右移动时，需要判断 newValue 是否大于target
        if ((speed < 0 && newValue < target) || (speed > 0 && newValue > target)){
            newValue = target;
        }
        //将新值设置给box1
        obj.style[attr] = newValue + "px";

        //当元素移动到80像素时，停止执行动画
        if (newValue == target){
            //到达目标，关闭定时器
            clearInterval(obj.timer);

            //动画执行完毕，调用回调函数
            callback && callback();
        }
    },30);
}




/*
* 定义一个函数，用来获取指定元素的当前样式
* 参数：
*   obj  要获取样式的元素
*   name  要获取的样式名
* */
function getStyle(obj,name) {

    if (window.getComputedStyle){
        //正常浏览器具有该方法
        return getComputedStyle(obj,null)[name];
    } else{
        //IE8
        return  obj.currentStyle[name];
    }
}





//定义一个函数用于向一个元素添加指class定属性值
/*
* 参数
*  obj   要添加class属性的元素
*  cn     要添加class属性
* */
function addClass(obj,cn) {

    //检查obj中是否含有cn
    if(!hasClass(obj,cn)){
        obj.className += " " + cn;
    }

}

/*
* 判断一个元素中是否含有指定class属性值
* */
function hasClass(obj,cn) {

    //判断obj是否含有cn属性
    //创建正则表达式
    //var reg = /\bcn\b/;
    var reg = new RegExp("\\b" + cn + "\\b");
    return reg.test(obj.className);

}

/*
* 删除一个元素中指定的class属性
* */
function delClass(obj,cn) {

    //创建一个正则表达式
    var reg = new RegExp("\\b" + cn + "\\b");

    //删除class
    obj.className = obj.className.replace(reg,"");//空串替换

}

/*
* toggleClass可以用来切换一个类，如果元素中具有该类就删除，没有就加上
* */
function toggleClass(obj,cn) {
    //判断是否含有cn
    if(hasClass(obj,cn)){
        //有则删除
        delClass(obj,cn);
    }else {
        //没有则添加
        addClass(obj,cn);
    }

}
